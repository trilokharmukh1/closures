let limitFunctionCallCount = require('../limitFunctionCallCount');

let result = limitFunctionCallCount(()=>{
    return "callback called";
},3);

console.log(result());
console.log(result());
console.log(result());
console.log(result());
console.log(result());