let cacheFunction = require('./../cacheFunction');

let result = cacheFunction((a,b) => {
    console.log("calling callback");
    return a+b;
})


console.log(result(10,12));
console.log(result(10,12));
console.log(result(10,112));