function cacheFunction(cb) {
    let cache = {};       // cache object which store argument as a key and result as a value

    return function invokeCallbackFunction() {

        if (cache.hasOwnProperty([[...arguments]])) {    // checking argument already seen or not
            return cache[[...arguments]]
        } else {
            let result = cb(...arguments);
            cache[[...arguments]] = result
            
            return cache[[...arguments]]
        }

    }

}

module.exports = cacheFunction;