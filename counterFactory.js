function counterFactory(){
    let counter = 0;     // closure

    increment = () => {
        return ++counter;   // increment counter when increment function called
    }

    decrement = () => {
        return --counter;   // decrement counter when decrement function called
    }

    return {increment:increment, decrement:decrement} // return object  
}

module.exports = counterFactory;