function limitFunctionCallCount(cb, n){
    let counter =1;              // use to count how many time function called   

    let invokesCallback = () => {
        if(counter++<=n){
            return cb();    
        }
        else{
            return null;
        }
    }

    return invokesCallback;     

}

module.exports = limitFunctionCallCount;
